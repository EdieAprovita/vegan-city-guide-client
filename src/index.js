import React from 'react';
import ReactDOM from 'react-dom';
import Router from './routes/App';
import { Provider } from 'react-redux';
import './styles/bootstrap.min.css';
import './styles/App.css';
import store from './redux/store';

ReactDOM.render(
	<Provider store={store}>
		<Router />
	</Provider>,
	document.getElementById('app')
);
