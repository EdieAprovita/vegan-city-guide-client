import { Container } from 'react-bootstrap';
import React, { Fragment } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Home from '../pages/Homepage';
import Register from '../pages/Login/Register';
import Businesses from '../pages/Business/Businesses';
import Doctors from '../pages/Doctor/Doctors';
import Markets from '../pages/Market/Markets';
import Professions from '../pages/Profession/Professions';
import Recipes from '../pages/Recipe/Recipes';
import Restaurants from '../pages/Restaurant/Restaurants';
import Navbar from '../components/Headers';
import Footer from '../components/Footer';

const App = () => {
	return (
		<Router>
			<Navbar />
			<main className='py-3'>
				<Switch>
					<Fragment>
						<Container>
							<Route exact path='/' component={Home} />
							<Route exact path='/signup' component={Register} />
							<Route path='/negocios' component={Businesses} />
							<Route path='/doctores' component={Doctors} />
							<Route path='/mercados' component={Markets} />
							<Route path='/profesiones' component={Professions} />
							<Route path='/recetas' component={Recipes} />
							<Route path='/restaurantes' component={Restaurants} />
						</Container>
					</Fragment>
				</Switch>
			</main>
			<Footer />
		</Router>
	);
};

export default App;
