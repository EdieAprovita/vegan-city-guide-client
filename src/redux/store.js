import { createStore } from "@reduxjs/toolkit";
import { applyMiddleware, combineReducers } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";

import {
	recipeCreateReducer,
	recipeDeleteReducer,
	recipeReviewCreateReducer,
	recipesDetailsReducer,
	recipesListReducer,
	recipeTopReviewReducer,
	recipeUpdateReducer,
} from "./recipesDucks";

import {
	userDeleteReducer,
	userDetailsReducer,
	userLoginReducer,
	userRegisterReducer,
	userUpdateProfileReducer,
	userUpdateReducer,
} from "./authDucks";

import {
	marketCreateReducer,
	marketDeleteReducer,
	marketReviewCreateReducer,
	marketsDetailsReducer,
	marketsListReducer,
	marketTopReviewReducer,
	marketUpdateReducer,
} from "./marketsDucks";

import {
	businessCreateReducer,
	businessDeleteReducer,
	businessesDetailsReducer,
	businessesListReducer,
	businessReviewCreateReducer,
	businessTopReviewReducer,
	businessUpdateReducer,
} from "./businessesDucks";

import {
	restaurantCreateReducer,
	restaurantDeleteReducer,
	restaurantReviewCreateReducer,
	restaurantsDetailsReducer,
	restaurantsListReducer,
	restaurantTopReviewReducer,
	restaurantUpdateReducer,
} from "./restaurantsDucks";

import {
	doctorCreateReducer,
	doctorDeleteReducer,
	doctorListReducer,
	doctorReviewCreateReducer,
	doctorsDetailsReducer,
	doctorTopReviewReducer,
	doctorUpdateReducer,
} from "./doctorsDucks";

import {
	professionCreateReducer,
	professionDeleteReducer,
	professionReviewCreateReducer,
	professionsDetailsReducer,
	professionsListReducer,
	professionTopReviewReducer,
	professionUpdateReducer,
} from "./professionDucks";

const rootReducer = combineReducers({
	userLogin: userLoginReducer,
	userRegister: userRegisterReducer,
	userDetails: userDetailsReducer,
	userUpdateProfile: userUpdateProfileReducer,
	userDelete: userDeleteReducer,
	userUpdate: userUpdateReducer,

	recipesList: recipesListReducer,
	recipesDetails: recipesDetailsReducer,
	recipeCreate: recipeCreateReducer,
	recipeUpdate: recipeUpdateReducer,
	recipeDelete: recipeDeleteReducer,
	recipeTop: recipeTopReviewReducer,
	recipeCreateReview: recipeReviewCreateReducer,

	marketsList: marketsListReducer,
	marketsDetails: marketsDetailsReducer,
	marketsCreate: marketCreateReducer,
	marketsUpdate: marketUpdateReducer,
	marketsDelete: marketDeleteReducer,
	marketsTop: marketTopReviewReducer,
	marketsCreateReview: marketReviewCreateReducer,

	businessesList: businessesListReducer,
	businessesDetails: businessesDetailsReducer,
	businessCreate: businessCreateReducer,
	businessUpdate: businessUpdateReducer,
	businessDelete: businessDeleteReducer,
	businessTop: businessTopReviewReducer,
	businessCreateReview: businessReviewCreateReducer,

	restaurantsList: restaurantsListReducer,
	restaurantDetail: restaurantsDetailsReducer,
	restaurantCreate: restaurantCreateReducer,
	restaurantUpdate: restaurantUpdateReducer,
	restaurantDelete: restaurantDeleteReducer,
	restaurantTop: restaurantTopReviewReducer,
	restaurantReview: restaurantReviewCreateReducer,

	doctorsList: doctorListReducer,
	doctorDetail: doctorsDetailsReducer,
	doctorCreate: doctorCreateReducer,
	doctorUpdate: doctorUpdateReducer,
	doctorDelete: doctorDeleteReducer,
	doctorTop: doctorTopReviewReducer,
	doctorCreateReview: doctorReviewCreateReducer,

	professionsList: professionsListReducer,
	professionDetail: professionsDetailsReducer,
	professionCreate: professionCreateReducer,
	professionUpdate: professionUpdateReducer,
	professionDelete: professionDeleteReducer,
	professionTop: professionTopReviewReducer,
	professionReview: professionReviewCreateReducer,
});

const userInfoFromStorage = localStorage.getItem("userInfo") ? JSON.parse(localStorage.getItem("userInfo")) : null;

const initialState = {
	userLogin: { userInfo: userInfoFromStorage },
};

const middleware = [thunk];

const store = createStore(rootReducer, initialState, composeWithDevTools(applyMiddleware(...middleware)));

export default store;
