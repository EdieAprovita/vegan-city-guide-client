import { Link } from 'react-router-dom';
import { Card } from 'react-bootstrap';
import Rating from './Rating';

const Business = ({ business }) => {
	return (
		<Card className='my-3 p-3 rounded'>
			<Link to={`/business/${business._id}`}>
				<Card.Img variant='top' src={business.image} />
			</Link>

			<Card.Body>
				<Link to={`/business/${business._id}`}>
					<Card.Title as='div'>
						<strong>{business.name}</strong>
					</Card.Title>
				</Link>

				<Card.Text as='div'>
					<Rating
						value={business.rating}
						text={`${business.numReviews} reviews`}
					/>
				</Card.Text>
				<CardText as='h3'>Dirección:{business.address}</CardText>
				<CardText as='h3'>Teléfono:{business.contact.phone}</CardText>
				<CardText as='h3'>Facebook:{business.contact.facebook}</CardText>
			</Card.Body>
		</Card>
	);
};

export default Business;
