import { Helmet } from 'react-helmet';

const Meta = ({ title, description, keywords }) => {
	return (
		<Helmet>
			<title>{title}</title>
			<meta name='description' content={description} />
			<meta name='keywords' content={keywords} />
		</Helmet>
	);
};

Meta.defaultProps = {
	title: 'Guia Vegana CDMX',
	description: 'Guia vegana de la ciudad de México',
	keywords: 'Restaurantes, Mercados,Recetas,Doctores, Profesionistas',
};

export default Meta;
