import { Link } from 'react-router-dom';
import { Card } from 'react-bootstrap';
import Rating from './Rating';

const Doctor = ({ doctor }) => {
	return (
		<Card className='my-3 p-3 rounded'>
			<Link to={`/doctor/${doctor._id}`}>
				<Card.Img variant='top' src={doctor.image} />
			</Link>

			<Card.Body>
				<Link to={`/doctor/${doctor._id}`}>
					<Card.Title as='div'>
						<strong>{doctor.name}</strong>
					</Card.Title>
				</Link>

				<Card.Text as='div'>
					<Rating value={doctor.rating} text={`${doctor.numReviews} Reseñas`} />
				</Card.Text>
				<Card.Text as='h3'>{doctor.specialty}</Card.Text>
			</Card.Body>
		</Card>
	);
};

export default Doctor;
