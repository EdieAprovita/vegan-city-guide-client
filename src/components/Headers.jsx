import { Navbar, Nav, Container, NavDropdown } from 'react-bootstrap';
import { Route } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { LinkContainer } from 'react-router-bootstrap';
import SearchBox from './SearchBox';
import { logoutAction } from '../redux/authDucks';

const Header = () => {
	const dispatch = useDispatch();

	const userLogin = useSelector(state => state.userLogin);
	const { userInfo } = userLogin;

	const logoutHandler = () => {
		dispatch(logoutAction());
	};
	return (
		<Navbar bg='dark' variant='dark' expand='lg' collapseOnSelect>
			<Container fluid>
				<LinkContainer to='/'>
					<Navbar.Brand className='ml-auto'>Guia CDMX</Navbar.Brand>
				</LinkContainer>
				<Navbar.Toggle aria-controls='basic-navbar-nav' />
				<Navbar.Collapse id='basic-navbar-nav'>
					<Route render={({ history }) => <SearchBox history={history} />} />

					<Nav className='ml-auto '>
						<LinkContainer to='/restaurantes'>
							<Nav.Link>
								<i className='fas fa-utensils px-2'></i>Restaurantes
							</Nav.Link>
						</LinkContainer>
						<LinkContainer to='mercados'>
							<Nav.Link>
								<i className='fas fa-shopping-cart px-2'></i>Mercados
							</Nav.Link>
						</LinkContainer>
						<LinkContainer to='/negocios'>
							<Nav.Link>
								<i className='fas fa-cash-register px-2'></i>Negocios
							</Nav.Link>
						</LinkContainer>
						<LinkContainer to='recetas'>
							<Nav.Link>
								<i className='fas fa-cookie-bite px-2'></i>Recetas
							</Nav.Link>
						</LinkContainer>
						<LinkContainer to='doctores'>
							<Nav.Link>
								<i className='fas fa-notes-medical px-2'></i>Doctores
							</Nav.Link>
						</LinkContainer>
						<LinkContainer to='profesiones'>
							<Nav.Link>
								<i className='fas fa-user-tie'></i>Profesiones
							</Nav.Link>
						</LinkContainer>
						{userInfo ? (
							<NavDropdown title={userInfo.name} id='username'>
								<LinkContainer to='/profile'>
									<NavDropdown.Item>Profile</NavDropdown.Item>
								</LinkContainer>
								<NavDropdown.Item onClick={logoutHandler}>
									Logout
								</NavDropdown.Item>
							</NavDropdown>
						) : (
							<LinkContainer to='/signup'>
								<Nav.Link>
									<i className='fas fa-user'></i> Sign Up
								</Nav.Link>
							</LinkContainer>
						)}
					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	);
};

export default Header;
